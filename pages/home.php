<?php
/**
 * Template name: Home
 */
?>
<?php
use Teik\Controller\HomeController;
use Timber\Timber;
get_header();
$context = Timber::get_context();
$context['post'] = Timber::get_post();
Timber::render('home.twig', $context);
get_footer();
?>
