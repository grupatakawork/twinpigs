<?php

namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;


class Banner extends AbstractBlock {
  use Singleton;

  public $name = 'banner';
  public $title = 'Banner';


  public function render($block, $content = '', $is_preview = false, $post_id = 0) {
    $options = get_fields('options');
    $fields  = get_fields();

    $context = array_merge(
      $fields ?: [],
      [
      'options'           => $options,
      ]
    );
    Timber::render('components/blocks/'.$this->name.'.twig', $context);
  }
}