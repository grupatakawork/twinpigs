<?php

namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;


class Drive extends AbstractBlock {
  use Singleton;

  public $name = 'drive';
  public $title = 'Drive';
}