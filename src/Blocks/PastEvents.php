<?php
namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;

class PastEvents extends AbstractBlock
{
  use Singleton;

  public $name = 'pastEvents';
  public $title = 'PastEvents';

  public function render($block, $content = '', $is_preview = false, $post_id = 0) {
    // $context = Timber::get_context();
    $fields = get_fields();
    global $paged;
    if (!isset($paged) || !$paged){
        $paged = 1;
    }
    $past_events = new \Timber\PostQuery([
      'post_type'       => 'past_events',
      'orderby'         => 'post_date',
      'order'           => 'ASC',
      'posts_per_page'  => 3,
      'paged' => $paged
    ]);
    $context = array_merge(
      $fields ?: [],
      [
      'events'           => $past_events,
      'post_id'         => $post_id,
      'is_preview'      => $is_preview
      ]
    );
    Timber::render('components/blocks/'.$this->name.'.twig', $context);
  }
}