<?php
namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;

class Zones extends AbstractBlock
{
  use Singleton;

  public $name = 'zones';
  public $title = 'Zones';

  public function render($block, $content = '', $is_preview = false, $post_id = 0) {
    // $context = Timber::get_context();
    $fields = get_fields();
    $zones = new \Timber\PostQuery([
      'post_type'       => 'zones',
      'orderby'         => 'post_date',
      'order'           => 'ASC',
      'posts_per_page'  => 15
    ]);
    $context = array_merge(
      $fields ?: [],
      [
      'zones'           => $zones,
      'post_id'         => $post_id,
      'is_preview'      => $is_preview
      ]
    );
    Timber::render('components/blocks/'.$this->name.'.twig', $context);
  }
}