<?php
namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;

class Trivia extends AbstractBlock
{
  use Singleton;

  public $name = 'trivia';
  public $title = 'Trivia';

  public function render($block, $content = '', $is_preview = false, $post_id = 0) {
    // $context = Timber::get_context();
    $fields = get_fields();
    $trivia = new \Timber\PostQuery([
      'post_type'       => 'curiosities',
      'orderby'         => 'post_date',
      'order'           => 'ASC',
      'posts_per_page'  => -1
    ]);
    $context = array_merge(
      $fields ?: [],
      [
      'trivia'           => $trivia,
      'post_id'         => $post_id,
      'is_preview'      => $is_preview
      ]
    );
    Timber::render('components/blocks/'.$this->name.'.twig', $context);
  }
}