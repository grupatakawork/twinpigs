<?php

namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;


class LightGallery extends AbstractBlock {
  use Singleton;

  public $name = 'lightGallery';
  public $title = 'LightGallery';
}