<?php
namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;

class News extends AbstractBlock
{
  use Singleton;

  public $name = 'news';
  public $title = 'News';

  public function render($block, $content = '', $is_preview = false, $post_id = 0) {
    // $context = Timber::get_context();
    $fields = get_fields();
    $news = new \Timber\PostQuery([
      'orderby'         => 'post_date',
      'order'           => 'ASC',
      'posts_per_page'  => 3
    ]);
    $context = array_merge(
      $fields ?: [],
      [
      'news'           => $news,
      'post_id'         => $post_id,
      'is_preview'      => $is_preview
      ]
    );
    Timber::render('components/blocks/'.$this->name.'.twig', $context);
  }
}