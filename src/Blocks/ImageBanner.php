<?php

namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;


class ImageBanner extends AbstractBlock {
  use Singleton;

  public $name = 'imageBanner';
  public $title = 'ImageBanner';


}