<?php
namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;

class Events extends AbstractBlock
{
  use Singleton;

  public $name = 'events';
  public $title = 'Events';

  public function render($block, $content = '', $is_preview = false, $post_id = 0) {
    // $context = Timber::get_context();
    $fields = get_fields();
    $events = new \Timber\PostQuery([
      'post_type'       => 'events',
      'orderby'         => 'post_date',
      'order'           => 'ASC',
      'posts_per_page'  => 3
    ]);
    $context = array_merge(
      $fields ?: [],
      [
      'events'           => $events,
      'post_id'         => $post_id,
      'is_preview'      => $is_preview
      ]
    );
    Timber::render('components/blocks/'.$this->name.'.twig', $context);
  }
}