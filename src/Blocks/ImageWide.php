<?php

namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;


class ImageWide extends AbstractBlock {
  use Singleton;

  public $name = 'imageWide';
  public $title = 'ImageWide';
}