<?php

namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;


class ImageFrameText extends AbstractBlock {
  use Singleton;

  public $name = 'imageFrameText';
  public $title = 'ImageFrameText';
}