<?php

namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;


class TitleText extends AbstractBlock {
  use Singleton;

  public $name = 'titleText';
  public $title = 'TitleText';
}