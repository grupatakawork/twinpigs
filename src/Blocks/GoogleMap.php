<?php

namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;


class GoogleMap extends AbstractBlock {
  use Singleton;

  public $name = 'googleMap';
  public $title = 'GoogleMap';
}