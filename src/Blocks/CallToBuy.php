<?php

namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;


class CallToBuy extends AbstractBlock {
  use Singleton;

  public $name = 'calltobuy';
  public $title = 'CallToBuy';
}