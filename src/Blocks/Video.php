<?php

namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;


class Video extends AbstractBlock {
  use Singleton;

  public $name = 'video';
  public $title = 'Video';
}