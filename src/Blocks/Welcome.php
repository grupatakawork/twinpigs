<?php

namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;


class Welcome extends AbstractBlock {
  use Singleton;

  public $name = 'welcome';
  public $title = 'Welcome';
}