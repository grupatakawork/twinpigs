<?php

namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;


class Contact extends AbstractBlock {
  use Singleton;

  public $name = 'contact';
  public $title = 'Contact';


  public function render($block, $content = '', $is_preview = false, $post_id = 0) {
    $context['options'] = get_fields('options');
    Timber::render('components/blocks/'.$this->name.'.twig', $context);
  }
}