<?php

namespace Teik\Blocks;

use Teik\Traits\Singleton;
use Timber\Timber;


class SeasonCalendar extends AbstractBlock {
  use Singleton;

  public $name = 'seasonCalendar';
  public $title = 'SeasonCalendar';
}