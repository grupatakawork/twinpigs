<?php
namespace Teik\Blocks;

$blocks = [
  Banner::instance(),
  ImageFrameText::instance(),
  Video::instance(),
  Zones::instance(),
  Events::instance(),
  PastEvents::instance(),
  News::instance(),
  Trivia::instance(),
  CallToBuy::instance(),
  ImageBanner::instance(),
  TitleText::instance(),
  ImageWide::instance(),
  WidgetBanner::instance(),
  Welcome::instance(),
  Drive::instance(),
  GoogleMap::instance(),
  Contact::instance(),
  SeasonCalendar::instance(),
  LightGallery::instance(),
];

foreach ($blocks as $key => $block) {
  $block->hook();
}