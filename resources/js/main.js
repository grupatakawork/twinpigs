import Calendar from "js-year-calendar";
import "js-year-calendar/dist/js-year-calendar.css";
import tippy from "tippy.js";
import "lightgallery.js/dist/js/lightgallery.js";

import $ from "jquery";
import { Menu } from "./components/Menu";
import Swiper from "swiper";

import "bootstrap/js/dist/util";
import "bootstrap/js/dist/modal";
import "bootstrap/js/src/collapse";
import "bootstrap/js/dist/collapse";
import "bootstrap/js/dist/tab";

import "masonry-layout/dist/masonry.pkgd";

import "./components/Swiper";
import "./components/Menu";
import "./components/Masonry";
import "./components/Accordion";
import "./components/Calendar";
import "./components/MonthCalendar";

Menu(".js-menu");

lightGallery(document.getElementById("animated-thumbnials"), {
  thumbnail: true,
  animateThumb: false,
  showThumbByDefault: false,
});
