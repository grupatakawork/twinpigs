import Swiper from "swiper";

var swiper = new Swiper(".js-swiper-banner", {
  effect: "fade",
  /*   autoplay: {
    delay: 4000,
  }, */
  navigation: {
    nextEl: ".swiper-next",
    prevEl: ".swiper-prev",
  },
  pagination: {
    el: ".swiper-pagination",
  },
  breakpoints: {
    1200: {
      spaceBetween: 30,
    },
  },
});
