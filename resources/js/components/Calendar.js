import $ from "jquery";
import "js-year-calendar/locales/js-year-calendar.pl";
import tippy from "tippy.js";

if ($("body").hasClass("page-id-13")) {
  document.addEventListener("DOMContentLoaded", function() {
    fetch("http://localhost/twinpigs/wp-json/wp/v2/reservations")
      .then((response) => response.json())
      .then((response) => {
        const AcfClosed = response[0].ACF.closed;
        const AcfClosedMaped = AcfClosed.map((i) => ({
          name: "test",
          startDate: new Date(i.date),
          endDate: new Date(i.date),
          color: "#e3051b",
        }));
        const AcfReservation = response[0].ACF.reservation;
        const AcfReservationMaped = AcfReservation.map((i) => ({
          name: "test",
          startDate: new Date(i.date),
          endDate: new Date(i.date),
          color: "#fff",
        }));
        const AcfEvents = response[0].ACF.events;
        const AcfEventsMaped = AcfEvents.map((i) => ({
          name: i.name,
          startDate: new Date(i.date),
          endDate: new Date(i.date),
          color: "#ffcc00",
        }));
        const JoinedEvenets = AcfClosedMaped.concat(AcfReservationMaped).concat(
          AcfEventsMaped
        );
        return JoinedEvenets;
      })
      .then((output) => {
        const reservations = output;
      });
  });

  let calendar = null;

  $(function() {
    var currentYear = new Date().getFullYear();

    calendar = new Calendar("#SeasonCalendar", {
      language: "pl",
      style: "background",
      minDate: new Date(currentYear, 3, 1),
      maxDate: new Date(currentYear, 9, 28),
      setStyle: "background",
      displayHeader: false,

      dataSource: function() {
        return fetch("http://localhost/twinpigs/wp-json/wp/v2/reservations")
          .then((response) => response.json())
          .then((response) => {
            const AcfClosed = response[0].ACF.closed;
            const AcfClosedMaped = AcfClosed.map((i) => ({
              startDate: new Date(i.date),
              endDate: new Date(i.date),
              color: "#e3051b",
            }));
            const AcfReservation = response[0].ACF.reservation;
            const AcfReservationMaped = AcfReservation.map((i) => ({
              startDate: new Date(i.date),
              endDate: new Date(i.date),
              color: "#fff",
            }));
            const AcfEvents = response[0].ACF.events;
            const AcfEventsMaped = AcfEvents.map((i) => ({
              name: i.name,
              startDate: new Date(i.date),
              endDate: new Date(i.date),
              color: "#ffcc00",
            }));
            const JoinedEvenets = AcfClosedMaped.concat(
              AcfReservationMaped
            ).concat(AcfEventsMaped);
            return JoinedEvenets;
          });
      },
      mouseOnDay: function(e) {
        if (e.events.length > 0) {
          let content = e.events[0].name;
          if (content) {
            if (!e.element._tippy) {
              tippy(e.element, {
                theme: "twinpigs",
                placement: "bottom",
                content: content,
                arrow: true,
              });
              e.element._tippy.show();
            }
          }
        }
      },
    });
  });

  document
    .querySelector("#SeasonCalendar")
    .addEventListener("renderEnd", function(e) {
      const legend = document.getElementById("CalendarLegend");
      legend.classList.add("season-calendar__legend--visiable");
    });
}
