import $ from "jquery";
import { Calendar } from "@fullcalendar/core";
import dayGridPlugin from "@fullcalendar/daygrid";
import plLocale from "@fullcalendar/core/locales/pl";

document.addEventListener("DOMContentLoaded", function() {
  let calendarEl = document.getElementById("monthCalendar");
  let calendarElSecond = document.getElementById("monthCalendarSecond");

  let obj;

  fetch("http://localhost/twinpigs/wp-json/wp/v2/reservations")
    .then((response) => response.json())
    .then((response) => {
      console.log(response);
      const AcfClosed = response[0].ACF.closed;
      const AcfClosedMaped = AcfClosed.map((i) => ({
        title: i.date,
        start: i.date,
        end: i.date,
        backgroundColor: "#e3051b",
        rendering: "background",
      }));
      const AcfReservation = response[0].ACF.reservation;
      const AcfReservationMaped = AcfReservation.map((i) => ({
        title: i.date,
        start: i.date,
        end: i.date,
        backgroundColor: "#fff",
        rendering: "background",
      }));
      const JoinedEvenets = AcfClosedMaped.concat(AcfReservationMaped);
      return JoinedEvenets;
    })
    .then((output) => {
      obj = output;
      const data = output;
      let calendar = new Calendar(calendarEl, {
        plugins: [dayGridPlugin],
        locale: plLocale,
        header: {
          left: "prev",
          center: "title",
          right: "next",
        },
        titleFormat: {
          month: "long",
        },
        columnHeader: false,
        firstDay: 1,
        showNonCurrentDates: false,
        fixedWeekCount: false,
        events: obj,
      });
      let calendarSecond = new Calendar(calendarElSecond, {
        plugins: [dayGridPlugin],
        locale: plLocale,
        header: {
          left: "prev",
          center: "title",
          right: "next",
        },
        titleFormat: {
          month: "long",
        },
        columnHeader: false,
        firstDay: 1,
        showNonCurrentDates: false,
        fixedWeekCount: false,
        events: obj,
        eventColor: "#e3051b",
        eventTextColor: "transparent",
      });
      $('#widget1 a[data-toggle="tab"]').on("shown.bs.tab", function(e) {
        calendar.render();
      });
      $('#widget2 a[data-toggle="tab"]').on("shown.bs.tab", function(e) {
        calendarSecond.render();
      });
    });
});
