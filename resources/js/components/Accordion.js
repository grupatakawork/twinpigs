import $ from "jquery";

$("#accordion").on("show.bs.collapse", function(e) {
  $(e.target)
    .siblings(".card__header")
    .addClass("show");
  $(e.target)
    .siblings(".card__title")
    .addClass("card__title--show");
});

$("#accordion").on("hide.bs.collapse", function(e) {
  $(e.target)
    .siblings(".card__header")
    .removeClass("show");
  $(e.target)
    .siblings(".card__title")
    .removeClass("card__title--show");
});

$("#trivia-accordion").on("show.bs.collapse", function(e) {
  $(e.target)
    .siblings(".card__header")
    .addClass("show");
  $(e.target)
    .siblings(".card__title")
    .addClass("card__title--show");
});

$("#trivia-accordion").on("hide.bs.collapse", function(e) {
  $(e.target)
    .siblings(".card__header")
    .removeClass("show");
  $(e.target)
    .siblings(".card__title")
    .removeClass("card__title--show");
});
