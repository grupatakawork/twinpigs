import $ from 'jquery';

export function Menu(element) {
  const OPEN_CLASS = 'is-open';
  let isOpen = false;
  let openElement = {
    trigger: null,
    target: null
  };
  const $element = $(element);

  const preventPropagation = event => {
    event.stopPropagation();
  }

  const bindClicks = (element) => {
    element.on('click', preventPropagation);
  }

  const unbindClicks = (element) => {
    element.off('click', preventPropagation);
  }

  const closeOthers = () => {
    if(openElement.trigger && openElement.target) {
      console.log('CLOSING OPENED ELEMENT', openElement);
    }
  }

  const setOpenElement = ($trigger, $target) => {
    openElement.trigger = $trigger;
    openElement.target = $target;
  }

  const toggle = ($trigger, $target, status) => {
    isOpen = !status;
    if(isOpen) {
      closeOthers();
      setOpenElement($trigger, $target);
      openElement.trigger.addClass(OPEN_CLASS);
      openElement.target.addClass(OPEN_CLASS);
      console.log(openElement.target);
      bindClicks($target);
    } else {
      if(openElement.trigger && openElement.target) {
        openElement.trigger.removeClass(OPEN_CLASS);
        openElement.target.removeClass(OPEN_CLASS);
        unbindClicks($target);

        openElement.trigger = null;
        openElement.target = null;
      }
    }
  }

  const handleClick = (event) => {
    event.stopPropagation();
    event.preventDefault();
    const $this = $(event.currentTarget);
    const $target = $($this.data('target'));

    console.log(isOpen);
    toggle($this, $target, isOpen);
  }

  const handleBodyClick = (event) => {
    if(isOpen) {
      toggle(openElement.trigger, openElement.target, isOpen);
    }
  }

  $(document.body).click(handleBodyClick);
  $element.click(handleClick);
}