<?php
  get_header();
  $context = \Timber\Timber::get_context();
  $context['posts'] = new \Timber\PostQuery();

  $queried = get_queried_object();

  \Timber\Timber::render(
    ['specialization.twig'],
    $context);
  get_footer();


?>
